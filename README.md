### **Description**
#### Task
Load "Out in Amsterdam" data from the attached data set. This set describes restaurants, cafes and bars in the Amsterdam region. Show the data
in tabular format and allow for applying filters.  
  
List of filters:  
- By name. Should allow filtering by substring (e.g. Gartine - Gart).  
- By city. It is possible to select several cities at once.  
- By start year.  
- By postcode.  
  
Table columns:  
- Name  
- City  
- Postcode  
- Address  
- Start year  
  
Click on a table row should reveal the restaurant's details: name, full address, URL and pictures.  

Bonuses not mentioned in this description.  Could be found in assignment folder.  
Whole description in here: [Homework-Webapplication](./assignment/Homework-Webapplication.pdf)

#### Status
Say hi to Angular for the first time - done  
Load data - done  
Show data - done  
Filter by name - done  
Filter by multiple cities - done  
Filter by postcode - done  

TODO:  
- Filter name by substring  
- Show (and filter) start year (first /dates/singles value?)  
- Not sure about zipcode - postcode difference  
- Component row-detail is missing  
- Bonuses untouched  
- Use more TS and not just JS  
- Save data to class and use class instead of JSON file  

---
### **Link**
served with Netlify:  
[https://admiring-meitner-a6eab1.netlify.com/](https://admiring-meitner-a6eab1.netlify.com/)

---
### **Technology**
JavaScript, NetliFy, Ant Design, Angular, GitLab CI/CD

---
### **Year**
2019

---
### **Screenshot**
![](./README/main.png)