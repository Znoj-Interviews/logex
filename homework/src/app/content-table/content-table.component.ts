import { Component } from "@angular/core";
import establishmentData from "../../../assets/data/establishment-data.json";

@Component({
  selector: "app-content-table",
  templateUrl: "./content-table.component.html"
})
export class ContentTableComponent {
  titleList = [];
  cityList = [];
  zipList = [];
  yearList = [];

  createListForFilter(data: string[]) {
    let list = [];
    for (let i = 0; i < establishmentData.length; i++) {
      let attribute = data[i];
      if (list.indexOf(attribute) < 0) {
        list.push(data[i]);
      }
    }
    return list.sort().map(attribute => {
      return {
        text: attribute,
        value: attribute
      };
    });
  }

  constructor() {
    this.titleList = this.createListForFilter(
      establishmentData.map(establishment => establishment.title)
    );
    this.cityList = this.createListForFilter(
      establishmentData.map(establishment => establishment.location.city)
    );
    this.zipList = this.createListForFilter(
      establishmentData.map(establishment => establishment.location.zipcode)
    );

    // yearList = establishmentData.map(establishment => {
    //   return { text: establishment.title, value: establishment.title };
    // });
  }

  sortTitle = null;
  sortValue = null;
  listOfSearchCities = [];
  searchTitle: string;
  searchZipcode: string;
  searchYear: string;
  data = establishmentData;
  displayData = [...this.data];

  sort(sort: { key: string; value: string }): void {
    this.sortTitle = sort.key;
    this.sortValue = sort.value;
    this.search();
  }

  filter(value: string, type: string): void {
    this[type] = value;
    this.search();
  }

  search(): void {
    /** filter data **/
    const filterFunc = item => {
      return (
        (this.searchTitle
          ? item.title.indexOf(this.searchTitle) !== -1
          : true) &&
        (this.listOfSearchCities.length
          ? this.listOfSearchCities.some(
              city => item.location.city.indexOf(city) !== -1
            )
          : true) &&
        (this.searchZipcode
          ? item.location.zipcode.indexOf(this.searchZipcode) !== -1
          : true)
      );
    };
    const data = this.data.filter(item => filterFunc(item));
    /** sort data **/
    if (this.sortTitle && this.sortValue) {
      this.displayData = data.sort((a, b) =>
        this.sortValue === "ascend"
          ? a[this.sortTitle] > b[this.sortTitle]
            ? 1
            : -1
          : b[this.sortTitle] > a[this.sortTitle]
          ? 1
          : -1
      );
    } else {
      this.displayData = data;
    }
  }
}
